################################################
# AWS Provider Plgn: tf12.26 - provider ~> 3.16
################################################
provider "aws" {
  version = "~>2.0"
  region  = var.aws_region
  profile = var.aws_profile
}

data "aws_availability_zones" "this_ds_azs" {}
data "aws_ami" "this_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-hvm*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

###########################
# Generate Random strings 
###########################
resource "random_integer" "this_random_vals" {
  min = 1000000
  max = 9999900
}

resource "aws_security_group" "this_fe_elb_sg" {
  name   = "public-lb-web-sg"
  vpc_id = var.vpc_id

  #Allow HTTP from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Allow HTTPS from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Outbound rules
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  #   tags = merge(
  #     local.common_tags, { Name = "${local.env_name}-elb-sg" }
  #   )
}

resource "aws_security_group" "this_fe_server_sg" {
  name   = "frontend-server-sg"
  vpc_id = var.vpc_id

  dynamic ingress {
    for_each = var.nginx_server_ports_ingress
    content {
      from_port   = ingress.key
      to_port     = ingress.key
      cidr_blocks = ingress.value
      protocol    = "tcp"
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "this_elb" {
  #   name = "${local.env_name}-frontend-elb"
  name = "public-fe-lb-web"

  subnets         = [var.subnet_id]
  security_groups = [aws_security_group.this_fe_elb_sg.id]
  instances       = aws_instance.this_nginx_servers[*].id

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  #   tags = merge(
  #     local.common_tags, { Name = "${local.env_name}-web-elb" }
  #   )
}

resource "aws_instance" "this_nginx_servers" {
  count                  = var.instance_count
  ami                    = var.aws_ami
  instance_type          = var.instance_type
  subnet_id              = var.subnet_id
  vpc_security_group_ids = [aws_security_group.this_fe_server_sg.id]
  key_name               = var.key_name
  #   iam_instance_profile   = aws_iam_instance_profile.nginx_profile.name
  #   depends_on             = [aws_iam_role_policy.allow_s3_all]
  tags = {
    "Name" = "web-${count.index}"
  }
}