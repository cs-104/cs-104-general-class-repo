variable "aws_region" {}
variable "vpc_id" {}
variable "subnet_id" {}
variable "instance_count" {}
variable "aws_profile" {}
variable "instance_type" {}
variable "key_name" {}
variable "aws_ami" {}
variable "subnet_priv_id" {}

variable "base_tomcat_server_ports_ingress" {
  # type = list 
  default = {
    "22" = ["0.0.0.0/0"]
    "80" = ["0.0.0.0/0"]
    "8080" = ["0.0.0.0/0"]
  }
}

variable "tomcat_server_ports_ingress" {
  # type = list 
  default = {
    "8080" = ["0.0.0.0/0"]
    "8081" = ["0.0.0.0/0"]
    "22" = ["0.0.0.0/0"]
  }
}

variable "priavte_lb_be_sg_ingress" {
  # type = list 
  default = {
    "8080" = ["0.0.0.0/0"]
    "8081" = ["0.0.0.0/0"]
  }
}
