#!/bin/bash
# AUTHOR: CS-104 team 
# Minimum script to run a report of aws services 
# Simple echo|aws commands for taking service reports 


NOW=$(date +"%m-%d-%Y")

echo ''
echo '******************************************************************************************************'
echo -e "Target service report as of $NOW "
echo '******************************************************************************************************'
echo ''
echo 'AWS Org and member accounts:'
aws organizations list-accounts --output table

echo 'Query all VPCs: '
aws ec2 describe-vpcs --output table
echo ''
echo 'AWS subnets within VPC:'
aws ec2 describe-subnets --query "Subnets[].SubnetId" --output table
echo ''
echo 'IamUsers:'
aws iam list-users --output table
echo ''
echo 'Query all S3 buckets'
aws s3api list-buckets --output table
echo ''
echo 'Route53 Hosted Zones:'
aws route53 list-hosted-zones --output table
echo ''
echo 'Query all running EC2 instances:'
aws ec2 describe-instances --output table


