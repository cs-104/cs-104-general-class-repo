# Declarative Pipeline Rules 

1. Running jenkins instance 
2. Jenkins version 2+ version 
3. Pipeline DSL plugin(Comes by default)
4. Starts with `pipeline block`
5. Gui configuration or Jenkinsfile(Simple Text)

After starting, the sample application retrieves content from the **default** Kentico Kontent sample project.

* * * 
http://jenkins.dev-minds.com/job/cs-104-general-class-lesson2/pipeline-syntax/

> Pipeline: `Madatory` for declarative piplines - starting point
```ruby
pipeline {

}
```

> Agents: `Mandatory` for declarative pipline - Instructs job to run on specified agent
```ruby
pipeline {
    agent any 
}
Other possible values
===================== 
 - any: Runs a Job or Stage on any available agent
 - none: Specification of agents per stage 
 - label: Run the job in agent which matches the label given here
 - docker: Runs a  job in given Docker container
```

> Stages block constitutes different executable stage blocks. At least one stage block is `mandatory` inside stages block
```ruby
pipeline {
    agent {
        label 'jk-slave-node'
    }
    stages {
        // Stage here 
    }
}
```

> Stage: `Mandatory` for declarative pipline - stage block contains the actual execution steps
```ruby
pipeline {
    agent any
    stages {
        stage('StartHere') {
           // Steps here
        }
    }
}
```

> Steps: `Mandatory` for declarative pipline - block contains the actual build step
```ruby
pipeline {
    agent any
    stages {
        stage('StartHere') {
           // Steps here
        }
    }
}
```

> Parameter: Provides a way for Jenkins job to interact with Jenkin users during the running of the build job.
```ruby
pipeline {
    agent any
    parameters {
        string(name: 'NAME', description: 'Please tell me your name?')
        text(name: 'DESC', description: 'Describe about the job details')
        booleanParam(name: 'SKIP_TEST', description: 'Want to skip running Test cases?')
        choice(name: 'BRANCH', choices: ['Master', 'Dev'], description: 'Choose branch')
        password(name: 'REMOTE_SERVER_PWD', description: 'Enter SONAR password')
    }
    stages {
        stage('SampleParamStage') {
            steps {
                echo "SayHello ${params.NAME}"
                echo "Job Details: ${params.DESC}"
                echo "Skip Running Test case ?: ${params.SKIP_TEST}"
                echo "Branch Choice: ${params.BRANCH}"
                echo "REMOTESERVER Password: ${params.REMOTE_SERVER_PWD}"
            }
        }
    }
}
```

> Script: helps us run Groovy|Bash code inside the Jenkins declarative pipeline
```ruby
pipeline {
    agent any
    parameters {
        string(name: 'NAME', description: 'Please tell me your name')
        choice(name: 'GENDER', choices: ['Male', 'Female'], description: 'Choose Gender')
    }
    stages {
        stage('Printing name') {
            steps {
                script {
                    def name = "${params.NAME}"
                    def gender = "${params.GENDER}"
                    if(gender == "Male") {
                        echo "Mr. $name"    
                    } else {
                        echo "Mrs. $name"
                    }
                }
            }
        }
   }
}
```


> Environemt: Key value pairs which helps passing values to job during job runtime from outside of Jenkinsfile. It’s one way of externalizing configuration.
```ruby
pipeline {
    agent any
    environment { 
        DEPLOY_TO = 'production'
    }
    stages {
        stage('Initialization') {
            steps {
                echo "${DEPLOY_TO}"
            }
        }
    }
}
--------------------------------------
pipeline {
    agent any
    stages {
        stage('Initialization') {
            environment { 
                   JOB_TIME = sh (returnStdout: true, script: "date '+%A %W %Y %X'").trim()
            }
            steps {
                sh 'echo $JOB_TIME'
            }
        }
    }
}
```

> Steps: Jenkins if condition simulator 
```ruby
pipeline {
     agent any
     stages {
         stage('InitializeJob') {
              when {
                  branch 'dev'             
              }
              steps {
                 echo "Working on dev branch"
              }
         }
     }
}
---------------------------------------
pipeline {
     agent any
     stages {
         stage('InitializeJob') {
              when { // groovy language 
                  expression {
                     return env.BRANCH_NAME == 'dev';
                  }             
              }
              steps {
                 echo "Working on dev branch"
              }
         }
     }
}
----------------------------------------
pipeline {
    agent any
    environment { 
        DEPLOY_TO = 'production'
    }
    stages {
        stage('InitializeJob') {
            when { 
                environment name: 'DEPLOY_TO', value: 'production'
            }
            steps { 
                echo 'Welcome to LambdaTest'
            }
        }
    }
}
---------------------------------------
pipeline {
    agent any
    environment { 
        DEPLOY_TO = 'production'
    }
    stages { //
        stage('InitializeJob') {
            when {   // multiple conditions & all conditions should be satisfied
                allOf { 
                    branch 'master'; 
                    environment name: 'DEPLOY_TO', value: 'production'
                } 
            }
            steps { 
                echo 'Welcome to LambdaTest'
            }
        }
    }
}
----------------------------------------
pipeline {
    agent any
    environment { 
        DEPLOY_TO = 'production'
    }
    stages {
        stage('Welcome Step') {
            when { //multiple conditions & any of the given conditions should be satisfied
                anyOf { 
                    branch 'master';
                    branch 'staging' 
                } 
            }
            steps { 
                echo 'Welcome to LambdaTest'
            }
        }
    }
}
```

> Tools: This block lets us add pre configured tools like Maven or Java to our job’s PATH. It’s an optional block.
```ruby
pipeline {
    agent any
    tools {
        maven 'MAVEN_PATH' 
    }
    stages {
         stage('Load Tools') {
              steps {
                 sh "mvn -version"
              }
         }
     }
}
```

> Parallel: parallel blocks allow us to run multiple stage blocks concurrently
```ruby
pipeline {
    agent any
    stages {
           stage('Parallel Stage') {
              parallel {
                stage('windows script') {
                    agent {
                        label "windows"
                    }
                    steps {
                        	echo "Running in windows agent"
		bat 'echo %PATH%'
                    }
                }
                stage('linux script') {
                    agent {
                        label "linux"
                    }
                    steps {
                       sh "Running in Linux agent"
                    }
                }
             }
        }
    }
}
```


> Triggers: Instead of triggering the build manually, we can configure build to run in certain time intervals using the triggers block
```ruby
pipeline {
    agent any
    triggers {
        cron('H/15 * * * *')
    }
    stages {
        stage('Example') {
            steps {
                echo 'Hello World'
            }
        }
    }
}
```

> Post: post block contains additional actions to be performed on completion of the pipeline execution. It can contain one or many of the following conditional blocks – always, changed, aborted, failure, success, unstable etc.
```ruby
pipeline {
     agent any
     stages {
         stage('build step') {
              steps {
                 echo "Build stage is running"
              }
         }
     }
     post {
         always {
             echo "You can always see me"
         }
         success {
              echo "I am running because the job ran successfully"
         }
         unstable {
              echo "Gear up ! The build is unstable. Try fix it"
         }
         failure {
             echo "OMG ! The build failed"
         }
     }
}
```