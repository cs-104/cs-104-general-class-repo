provider "aws" {
  region = "eu-west-1"
  profile = "dmf-sandbox-adm"
}

locals {
  cluster_name = "cs-104-cluster"
}

module "eks" {
  source          = "git::https://github.com/terraform-aws-modules/terraform-aws-eks.git?ref=v12.1.0"
  cluster_name    = local.cluster_name
  vpc_id          = "vpc-047dc1c4077ef720b"
  subnets         = ["subnet-0cc1a2b959ad58962", "subnet-0584a0f419a255b69"]
  cluster_version = "1.20"

  node_groups = {
    eks_nodes = {
      desired_capacity = 2
      max_capacity     = 4
      min_capaicty     = 1
      instance_type    = "t2.small"

      k8s_labels = {
        Environment = "SandBox104Cluster"
      }
    }
  }

  manage_aws_auth = false
  map_accounts = var.map_accounts

  tags = {
    Environment = "SandBox104Cluster"
    Name = "StagingSandbox"
    Managedby = "Terraform"
  }
}

output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks.cluster_endpoint
}
