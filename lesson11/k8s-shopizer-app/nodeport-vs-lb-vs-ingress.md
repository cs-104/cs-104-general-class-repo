https://matthewpalmer.net/kubernetes-app-developer/articles/kubernetes-ingress-guide-nginx-example.html
NODEPORT: 
- Kubernetes will allocate "a" specific port on each Node to that service, and any request to your cluster on that port gets forwarded to the service.

LOADBALANCER 
- Every time you want to expose "a" service to the outside world, you have to create a new LoadBalancer and get an IP address.

INGRESS
- 