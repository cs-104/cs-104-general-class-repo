provider "aws" {
    region = "eu-west-1"
}

resource "aws_iam_user" "su" {
  name = "sample-user"
  path = "/"

  tags = {
    "Name" = "sample-user"
  }
}