provider "aws" {
  region = "eu-west-1"
}

resource "aws_s3_bucket" "my-bk-res" {
  bucket = "dmindz-sample-bucketz"
  acl    = "private"

  tags = {
    Name        = "DmindsSampleBucket"
    Environment = "Dev"
  }
}