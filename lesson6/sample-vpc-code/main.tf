provider "aws" {
    region = "eu-west-1"
}

resource "aws_vpc" "vpc_demo" {
  cidr_block                       = "10.0.25.0/24"
  instance_tenancy                 = "default"
  enable_dns_hostnames             = false
  enable_dns_support               = false
  enable_classiclink               = false

  tags = {
      Name = "dmins-samplevpc"
    }

}