#¡/bin/bash 

# Lesson-6: Part1 
# Create an ec2 instance on default VPC with awscli    

#*****************************************************************#
# Variouse functions per resource query 
#*****************************************************************#
ProjectName="cs-104-lesson6-fmbah"
KEY_PAIR_NAME="centrale-keys"
SERVER_COUNT="1"
SERVER_TYPE="t2.micro"
REGION="eu-west-1"


fetch_default_vpc_id(){
    default_vpc_id=`aws ec2 describe-vpcs \
    --filters "Name=is-default,Values=true" \
    --query "Vpcs[*].VpcId" \
    --output text`
}

fetch_default_subnet_id(){
    default_sb_id=`aws ec2 describe-subnets \
    --filters "Name=vpc-id,Values=${default_vpc_id}" \
    --query "Subnets[1].SubnetId" \
    --output text`
}

fetch_default_security_grp(){
    default_sg_grp_id=`aws ec2 describe-security-groups \
    --filter "Name=vpc-id,Values=${default_vpc_id}" \
    --query "SecurityGroups[1].GroupId" \
    --output text`
} 

fetch_latest_ami_id(){
    centos_latest_ami=`aws ec2 describe-images \
    --owners 'aws-marketplace' \
    --filters 'Name=product-code,Values=aw0evgkw8e5c1q413zgy5pjce' \
    --query 'sort_by(Images, &CreationDate)[-1].[ImageId]' \
    --output 'text'`
}

all_required_values_for_ec2(){
    echo -e "ALL REQUIRED VALUES FOR EC2 CREATION\n****************************************"
    echo -e "VPC-ID=${default_vpc_id}"
    echo -e "SB-ID=${default_sb_id}"
    echo -e "SG-ID=${default_sg_grp_id}"
    echo -e "AMI-ID=${centos_latest_ami}"
    echo -e "KEY-NAME=${KEY_PAIR_NAME}"
}

launch_server(){
    launch_my_server=`aws ec2 run-instances \
    --image-id "${centos_latest_ami}" \
    --count "${SERVER_COUNT}" \
    --instance-type "${SERVER_TYPE}" \
    --key-name "${KEY_PAIR_NAME}" \
    --security-group-ids "${default_sg_grp_id}" \
    --subnet-id "${default_sb_id}" \
    --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value='${ProjectName}'-server}]'`
}


fetch_default_vpc_id
fetch_default_subnet_id
fetch_default_security_grp
fetch_latest_ami_id
fetch_latest_ami_id
all_required_values_for_ec2
launch_server



