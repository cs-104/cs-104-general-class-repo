variable "aws_ami" {
  default = "ami-0b850cf02cc00fdc8"
}

variable "server_type" {
  default = "t2.micro"
}

variable "target_keypairs" {
  default = "dm-kliuch"
}



resource "aws_security_group" "dm_sg_res" {
  name        = "${var.which_env_nam_tag}-server-sg"
  description = "multiple firewall rules"
  vpc_id      = aws_vpc.dm_vpc_res.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_vpc.dm_vpc_res, aws_subnet.dm_pub_res
  ]
}

resource "aws_instance" "server_inst_res" {
  ami                    = var.aws_ami
  instance_type          = var.server_type
  vpc_security_group_ids = [aws_security_group.dm_sg_res.id]
  key_name               = var.target_keypairs
  subnet_id              = aws_subnet.dm_pub_res.*.id[1]

  #   connection {
  #     type        = "ssh"
  #     user        = "centos"
  #     private_key = "${file("./YOUR_KEY_NAME")}"
  #     timeout     = "3m"
  #     host        = "${self.public_ip}"
  #     #host        = "${self.private_ip}"
  #   }

  #   provisioner "remote-exec" {
  #     inline = [
  #       "sudo yum update -y"
  #     ]
  #   }

  tags = {
    Name = "${var.which_env_nam_tag}-server"
  }
}

# output "pub_ip" {
#   value = ["${aws_instance.blueprint_inst_res.public_ip}"]
# }