provider "aws" {
    region = "eu-west-1"
}

resource "aws_instance" "instance" {
  ami                         = ""
  instance_type               = ""
  associate_public_ip_address = ""
  vpc_security_group_ids      = ""
  subnet_id                   = ""
  key_name                    = ""
 
 
  tags = {
    "Owner"               = var.owner
    "Name"                = ""
  }
}
