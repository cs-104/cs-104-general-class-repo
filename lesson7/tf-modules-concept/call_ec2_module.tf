provider "aws" {
  region = "eu-west-1"
}

terraform {
  required_version = ">= 0.12.31"

  backend "s3" {
    bucket  = "dm-vpc-states"
    key = "lesson7/module-concepts/terraform.tfstate"
    region  = "eu-west-1"
    encrypt = "true"
    # workspace_key_prefix = "compute-resource-state"
  }
}

module "servers" {
  source = "git::https://fmbah_bb@bitbucket.org/cs-104/tf-ec2-module.git"

  acct_vpc_id    = "vpc-0acd7ad1af977e3f0"
  acct_subnet_id = "subnet-084e2f75d851cb049"
  env_name       = "preprod"
  aws_ami        = "ami-0b850cf02cc00fdc8"
  server_type    = "t2.micro"
  target_keypairs = "preprod"
}
