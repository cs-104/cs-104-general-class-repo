# STARTHERE WITH DOCKER 

1. Install docker engine
    - Windows: https://www.docker.com/products/docker-desktop
    - MacOs: https://www.docker.com/products/docker-desktop
    - Linux: https://docs.docker.com/engine/install/centos/
        - For linux start and enable docker 
        - add your user tock docker group 
        - systemctl start docker 
> Which ever you go with, we already have `docker engine` installed on our bastion node 
```ruby
[centos@bastion-node ~]$ docker version 
Client: Docker Engine - Community
 Version:           20.10.7
 API version:       1.41
```



2. Test your installation 
    - to confirm docker is correctly installed run a simple `helloworld` 
> Sample docker helloworld     
```ruby
[centos@bastion-node ~]$ docker run hello-world 
Hello from Docker!
This message shows that your installation appears to be working correctly.
```

3. Create docker hub account(free) - this is equivalent to aws ECR(Elastic container registry)
    - login to your docker-hub account with `docker login` command
> Logon to docker registry(docker hub)
```ruby    
lesson10[deploy ?] $ docker login 
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: phelun
Password: 
Login Succeeded 
```


4. Docker Verbs 
    - run 
    - pull 
    - exec 
    - inspect 
    - start|stop 
    - push 
    - ps 

5. Docker images (Variouse methods)
    - Build from container 
        - `docker commmit 'container-name' `
        - `docker tag 'container-id' 'new-name'`
        - `docker stop 'old-container'`
        - `docker push 'new-container'` 
    - Pull online 
    - Dockerfile - Automated

6. Dockerfile (Scripted)
    - Contains a list of commands that the Docker client calls while creating an image
    - you don't really have to learn new syntax to create your own dockerfiles.
    - Common Dockerfile directives - Reference --> https://docs.docker.com/engine/reference/builder/#from
        - FROM
        - WORKDIR
        - COPY
        - RUN
        - EXPOSE
        - CMD
    > Sample Dockerfile 
    ```ruby
    FROM centos:centos7
    LABEL maintainer="cs-104"

    ENV nginxversion="1.14.0-1" \
        os="centos" \
        osversion="7" \
        elversion="7_4"

    RUN yum install -y wget openssl sed &&\
        yum -y autoremove &&\
        yum clean all &&\
        wget http://nginx.org/packages/$os/$osversion/x86_64/RPMS/nginx-$nginxversion.el$elversion.ngx.x86_64.rpm &&\
        rpm -iv nginx-$nginxversion.el$elversion.ngx.x86_64.rpm

    COPY nginx.conf /etc/nginx/nginx.conf
    COPY index.html /data/www/index.html
    VOLUME [ "/data/www" ]
    EXPOSE 80

    CMD ["nginx", "-g", "daemon off;"]
    ```
    - Docker Build|Tag|Push
        - `docker build .` 


7. Multi-Container Environment(AKA Container Ochestration)
    - DockerCompose | kubernetes(AKS|EKS|Kubeadm|Kops|KubeSpray) | DockerSwarm | ECS 
        - Install docker compose binary - https://docs.docker.com/compose/install/
        - command always requires `dockercompose.yml` file to run 
        - specify a file with `-f option`
        - Variouse ways to run a docker compose based app 
            - `docker-compose .`  - has a single dockercompose.yaml file
            - `docker-compose up -d --build .` 
            - `docker-compose -f whatever.yaml`
    > Sample docker-compose wordpress app 
    ```ruby
        version: '3'

        networks:
        wordpress:


        #**************************************************************#
        # All services definition 
        # Each service has its own container 
        # Each service optionally has its dockerfile-which will be built
        # 
        #***************************************************************#    
        services:

        #***************************************************#
        # Frontend webserver 
        # Nginx webserver 
        #***************************************************#
        site:
            build:
            context: .
            dockerfile: nginx.dockerfile
            container_name: nginx_webserver_frontend
            ports:
            - 80:80
            - 443:443
            volumes:
            - ./wordpress:/var/www/html:delegated
            depends_on:
            - php
            - mysql
            networks:
            - wordpress

        
        #***************************************************#
        # Database service 
        #***************************************************#
        mysql:
            image: mysql:5.7.29
            container_name: mysql
            restart: always
            ports:
            - 3306:3306
            environment:
            MYSQL_DATABASE: wp
            MYSQL_USER: wp
            MYSQL_PASSWORD: secret
            MYSQL_ROOT_PASSWORD: secret
            SERVICE_TAGS: dev
            SERVICE_NAME: mysql
            networks:
            - wordpress

        #***************************************************#
        # All PHP dependencies 
        #***************************************************#
        php:
            build:
            context: .
            dockerfile: php.dockerfile
            container_name: php
            volumes:
            - ./wordpress:/var/www/html:delegated
            networks:
            - wordpress

        #***************************************************#
        # cordpress cli 
        #***************************************************#
        wp:
            build:
            context: .
            dockerfile: php.dockerfile
            container_name: wp
            entrypoint: ['wp', '--allow-root']
            volumes:
            - ./wordpress:/var/www/html:delegated
            networks:
            - wordpress
        ```


