provider "aws" {
  region = "eu-west-1"
}

variable "aws_ami" {
  default = "ami-0b850cf02cc00fdc8"
}

variable "server_type" {
  default = "t2.micro"
}

variable "target_keypairs" {
  default = "centrale-keys"
}

variable "project" {
  default = "fmbah-iac-cm"
}

variable "vpc_id" {
  default = "vpc-0e82695bfd0c584ed"
}

variable "pub_subnet_id" {
  default = "subnet-08eba39c6ffef0000"
}



resource "aws_security_group" "dm_sg_res" {
  name        = "${var.project}-server-sg"
  description = "multiple firewall rules"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "server_inst_res" {
  ami                    = var.aws_ami
  instance_type          = var.server_type
  vpc_security_group_ids = [aws_security_group.dm_sg_res.id]
  key_name               = var.target_keypairs
  subnet_id              = var.pub_subnet_id

    connection {
      type        = "ssh"
      user        = "centos"
      private_key = "${file("../../../../centrale-keys.pem")}"
      timeout     = "3m"
      host        = "${self.public_ip}"
    }

    provisioner "remote-exec" {
      inline = [
        "sudo yum update -y",
        "sudo yum install git wget vim unzip -y",
        "sudo setenforce 0"
      ]
    }

    provisioner "local-exec" {
        command = "ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -u centos -i '${self.public_ip},' --private-key '../../../../centrale-keys.pem' site.yaml"
    }

  tags = {
    Name = "${var.project}-server"
  }
}

output "pub_ip" {
  value = ["${aws_instance.server_inst_res.public_ip}"]
}