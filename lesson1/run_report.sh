#!/bin/bash

# Author: YourName 
# Team: CS-104 
# Documentation: See Readme.md 

# The script calls the actual fetched data 
# In bash script its good practice to have a `common` script 
# where functions are defined so you have another script calling it 

# Here in the run_report.sh script, you can further improve your report
# touching the actual script that does the data query. Its a common practice 

/home/centos/cs-104-general-class-repo/lesson1/common.sh | tee /home/centos/cs-104-general-class-repo/lesson1/reports-`date '+%Y|%m|%d-%H:%M'`.log


